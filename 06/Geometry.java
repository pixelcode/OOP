import java.lang.Math;

abstract class Figure {
  protected double x1,x2,y1,y2;

  public Figure(){
    x1 = x2 = y1 = y2 = 0;
  }

  public Figure(double x1, double x2, double y1, double y2){
    this.x1 = Math.min(x1,x2);
    this.x2 = Math.max(x1,x2);

    this.y1 = Math.min(y1,y2);
    this.y2 = Math.max(y1,y2);
  }

  double[] getX(){
    double[] x = new double[2];
    x[0] = this.x1;
    x[1] = this.x2;
    return x;
  }

  double[] getY(){
    double[] y = new double[2];
    y[0] = this.y1;
    y[1] = this.y2;
    return y;
  }

  void print(){
    System.out.println("x1: " + this.x1 + "; x2: " + this.x2 + "; y1: " + this.y1 + "; y2: " + this.y2);
  }
}


interface MobileObject {
  void move(double x, double y);
  void increase(double scale);
  void decrease(double scale);
}


class Rectangle extends Figure implements MobileObject {
  private double width, height;

  Rectangle(){
    super();
    this.width = Math.abs(this.x1 - this.x2);
    this.height = Math.abs(this.y1 - this.y2);
  }

  Rectangle(double x1, double y1, double width, double height){
    super(x1, x1 + width, y1, y1 + width); // call to super must be first statement in constructor.....
    this.width = width;
    this.height = height;
  }

  double getWidth(){
    return this.width;
  }

  double getHeight(){
    return this.height;
  }

  void print(){
    System.out.println("x1: " + this.x1 +
                     "; x2: " + this.x2 +
                     "; y1: " + this.y1 +
                     "; y2: " + this.y2 +
                     "; width: " + this.width +
                     "; height: " + this.height);
  }

  public void move(double x, double y){ // https://coderanch.com/t/653828/java/Error-java-code-implement
    this.x1 = this.x1 + x;
    this.x2 = this.x2 + x;
    this.y1 = this.y1 + y;
    this.y2 = this.y2 + y;
  }

  public void increase(double scale){
    this.width = this.width * scale;
    this.height = this.height * scale;

    this.x2 = this.x1 + this.width;
    this.y2 = this.y1 + this.height;
  }

  public void decrease(double scale){
    this.width = this.width / scale;
    this.height = this.height / scale;

    this.x2 = this.x1 + this.width;
    this.y2 = this.y1 + this.height;
  }

  String planarAnalysis(Rectangle anaRect){
    if (anaRect.width == this.width &&
        anaRect.height == this.height &&
        anaRect.x1 == this.x1 &&
        anaRect.x2 == this.x2 &&
        anaRect.y1 == this.y1 &&
        anaRect.y2 == this.y2
      ){
      return "same";
    }

    if (anaRect.width <= this.width &&
        anaRect.height <= this.height &&
        anaRect.x1 >= this.x1 &&
        anaRect.x2 <= this.x2 &&
        anaRect.y1 >= this.y1 &&
        anaRect.y2 <= this.y2
      ){
        // <= bzw. => ist hier erlaubt, da Gleichheit
        // bereits untersucht wurde
      return "contained";
    }

    if (anaRect.x1 == this.x2 ||
        anaRect.x2 == this.x1 ||
        anaRect.y1 == this.y2 ||
        anaRect.y2 == this.y1
      ){
      return "aligned";
    }

    if (anaRect.x2 < this.x1 ||
        anaRect.y2 < this.y1 ||
        anaRect.x1 > this.x2 ||
        anaRect.y1 > this.y2
      ){
      return "disjoint";
    }

    return "overlapping, but neither same nor contained";
  }
}


class Geometry {
  public static void main(String[] args){
    Rectangle r1, r2, r3, r4, r5, r6;

    r1 = new Rectangle(1, 2, 5, 5);
    r2 = new Rectangle(1, 2, 5, 5);
    r3 = new Rectangle(2, 3, 2, 1);
    r4 = new Rectangle(-4.5, -8.1, 2, 6);
    r5 = new Rectangle(-3, -2, 4, 4);
    r6 = new Rectangle(2, 2, 9, 10);

    System.out.print("r1:   "); r1.print();
    System.out.print("r2:   "); r2.print();
    System.out.print("r3:   "); r3.print();
    System.out.print("r4:   "); r4.print();
    System.out.print("r5:   "); r5.print();
    System.out.print("r6:   "); r6.print();
    System.out.println();

    System.out.println("width of r1: " + r1.getWidth());
    System.out.println("height of r2: " + r1.getHeight());
    System.out.println();
    System.out.println("r1 and r2: " + r1.planarAnalysis(r2));
    System.out.println("r1 and r3: " + r1.planarAnalysis(r3));
    System.out.println("r1 and r4: " + r1.planarAnalysis(r4));
    System.out.println("r1 and r5: " + r1.planarAnalysis(r5));
    System.out.println("r1 and r6: " + r1.planarAnalysis(r6));
    System.out.println();

    r1.increase(2);
    System.out.println("width of r1 (after increase): " + r1.getWidth());
    System.out.println("height of r2 (after increase): " + r1.getHeight());
    System.out.println();

    r1.decrease(4);
    System.out.println("width of r1 (after decrease): " + r1.getWidth());
    System.out.println("height of r2 (after decrease): " + r1.getHeight());
  }
}