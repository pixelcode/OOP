import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.lang.Math;
import java.lang.String;
import java.util.Scanner;
import java.util.Objects;

class Roman {
  private int numberInt;
  private String numberString;

  static String[] romans;
  static List<String> romansList;
  static int[] decimals;


  Roman(int number){
    initNumberHMs();
    if ( !(Roman.validate(number)) ) return;

    this.numberInt = number;
    this.numberString = this.IntToRoman(number);
  }


  Roman(String number){
    initNumberHMs();
    if ( !(Roman.validate(number)) ) return;

    this.numberString = number;
    this.numberInt = this.RomanToInt(number);
  }


  void initNumberHMs(){
    this.romans = new String[] { "I", "IV", "V", "IX", "X", "XL", "L", "XC", "C", "CD", "D", "CM", "M" };
    this.romansList = new ArrayList<>(Arrays.asList(romans));
    this.decimals = new int[] { 1, 4, 5, 9, 10, 40, 50, 90, 100, 400, 500, 900, 1000 };
  }

  static boolean validate(int number){
    if (number < 1 || number > 3999){ // MMMCMXCIX == 3999
      System.out.println("Römische Zahlen müssen im Intervall [1, 3999] liegen!");
      return false;
    }

    return true;
  }


  static boolean validate(String number){
    int i;

    for (i = 0; i < number.length(); i++){
      if ( !(romansList.contains( Character.toString( number.charAt(i) )))) {
        System.out.println("Römische Zahl enthält mindestens ein ungültiges Zeichen (" + number.charAt(i) + ")!");
        return false;
      }
    }

    return true;
  }


  String IntToRoman(int number){
    if ( !(validate(number)) ) return "null";

    // römische Zahlen aufsteigend einsetzen,
    // nach jeder röm. Zahl die "nächstkleinere Subtraktionsausnahme" einsetzen
    return "I".repeat(number)
              .replace("IIIII", "V")  // 5
              .replace("IIII", "IV")  // 4
              .replace("VV", "X")     // 10
              .replace("VIV", "IX")   // 9
              .replace("XXXXX", "L")  // 50
              .replace("XXXX", "XL")  // 40
              .replace("LL", "C")     // 100
              .replace("LXL", "XC")   // 90
              .replace("CCCCC", "D")  // 500
              .replace("CCCC", "CD")  // 400
              .replace("DD", "M")     // 1000
              .replace("DCD", "CM");  // 900
  }


  int RomanToInt(String number){
    if ( !(validate(number)) ) return -1;

    int i, numberInt;
    numberInt = 0;

    for (i = 0; i < number.length() - 1; i++){
      String currentChar = Character.toString( number.charAt(i) );
      int indexOfChar = Arrays.asList(this.romans).indexOf( currentChar );
      int currentDecimal = this.decimals[indexOfChar];

      String successorChar = Character.toString( number.charAt(i+1) );
      int indexOfSuccChar = Arrays.asList(this.romans).indexOf( successorChar );
      int successorDecimal = this.decimals[indexOfSuccChar];

      if (currentDecimal >= successorDecimal) {
        numberInt += currentDecimal;
      } else {
        numberInt -= currentDecimal;
      }
    }

    // letzte römische Ziffer hat keinen Nachfolger, darum separate Behandlung
    int lastIndex = number.length() - 1;
    String lastChar = Character.toString( number.charAt(lastIndex) );
    int indexOfLastChar = Arrays.asList(this.romans).indexOf(lastChar);
    numberInt += this.decimals[ indexOfLastChar ];

    return numberInt;
  }


  Roman addition(Roman number){
    int normalResult = this.numberInt + number.numberInt;
    Roman romanResult = new Roman(normalResult);
    return romanResult;
  }


  Roman subtraction(Roman number){
    int normalResult = this.numberInt - number.numberInt;
    Roman romanResult = new Roman(normalResult);
    return romanResult;
  }


  Roman multiplication(Roman number){
    int normalResult = this.numberInt * number.numberInt;
    Roman romanResult = new Roman(normalResult);
    return romanResult;
  }


  Roman division(Roman number){
    int normalResult = (int) (this.numberInt / number.numberInt);
    Roman romanResult = new Roman(normalResult);
    return romanResult;
  }


  String getRoman(){
    return this.numberString;
  }


  int getInt(){
    return this.numberInt;
  }


  @Override public String toString(){
    return "Zahl " + this.hashCode() + ": römische Darstellung = " + this.numberString + " | Dezimaldarstellung = " + this.numberInt;
  }


  @Override public int hashCode(){
    // https://mkyong.com/java/java-how-to-overrides-equals-and-hashcode
    int result = 17;
        result = 31 * result + this.numberString.hashCode();
        result = 31 * result + this.numberInt;
    return result;
  }


  boolean equals(Roman number){
    return number.hashCode() == this.hashCode();
  }
}

public class RomanNumbers {
  public static void main(String[] args){
    Roman[] romanArray = new Roman[12];
    int i;

    for (i = 0; i < 8; i++){
      int random = (int) Math.floor(Math.random() * 2000);
      romanArray[i] = new Roman(random);
    }

    romanArray[8]  = romanArray[0].addition(romanArray[1]);
    romanArray[9]  = romanArray[2].subtraction(romanArray[3]);
    romanArray[10] = romanArray[4].multiplication(romanArray[5]);
    romanArray[11] = romanArray[6].division(romanArray[7]);

    for (i = 0; i < 8; i++){
      System.out.println(romanArray[i].toString());
    }

    System.out.println();

    System.out.println(romanArray[0].getInt() + " + " + romanArray[1].getInt() + " = " + romanArray[8].getInt());
    System.out.println(romanArray[2].getInt() + " - " + romanArray[3].getInt() + " = " + romanArray[9].getInt());
    System.out.println(romanArray[4].getInt() + " * " + romanArray[5].getInt() + " = " + romanArray[10].getInt());
    System.out.println(romanArray[6].getInt() + " / " + romanArray[7].getInt() + " = " + romanArray[11].getInt());

    System.out.println();

    System.out.println(romanArray[0].getRoman() + " + " + romanArray[1].getRoman() + " = " + romanArray[8].getRoman());
    System.out.println(romanArray[2].getRoman() + " - " + romanArray[3].getRoman() + " = " + romanArray[9].getRoman());
    System.out.println(romanArray[4].getRoman() + " * " + romanArray[5].getRoman() + " = " + romanArray[10].getRoman());
    System.out.println(romanArray[6].getRoman() + " / " + romanArray[7].getRoman() + " = " + romanArray[11].getRoman());
  }
}