import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.lang.Math;
import java.util.Scanner;

class Roman {
  private int numberInt;
  private String numberString;

  static String[] romans;
  static List<String> romansList;
  static int[] decimals;


  Roman(int number){
    initNumberHMs();
    if ( !(Roman.validate(number)) ) return;

    this.numberInt = number;
    this.numberString = this.IntToRoman(number);
  }


  Roman(String number){
    initNumberHMs();
    int i;

    if ( !(Roman.validate(number)) ) return;

    this.numberString = number;
    this.numberInt = this.RomanToInt(number);
  }


  void initNumberHMs(){
    this.romans = new String[] { "I", "IV", "V", "IX", "X", "XL", "L", "XC", "C", "CD", "D", "CM", "M" };
    this.romansList = new ArrayList<>(Arrays.asList(romans));
    this.decimals = new int[] { 1, 4, 5, 9, 10, 40, 50, 90, 100, 400, 500, 900, 1000 };
  }


  static boolean validate(int number){
    if (number < 1 || number > 3999){ // MMMCMXCIX == 3999
      System.out.println("Römische Zahlen müssen im Intervall [1, 3888] liegen!");
      return false;
    }

    return true;
  }


  static boolean validate(String number){
    int i;

    for (i = 0; i < number.length(); i++){
      if ( !(romansList.contains( Character.toString( number.charAt(i) )))) {
        System.out.println("Römische Zahl enthält mindestens ein ungültiges Zeichen (" + number.charAt(i) + ")!");
        return false;
      }
    }

    return true;
  }


  String IntToRoman(int number){
    if ( !(validate(number)) ) return "null";
    int tempNumber = number;
    String result = "";

    int i = 1;

    while (number > 0){
      tempNumber = number;
      int tenPow = (int) Math.pow(10,i);
      number = ((int) (number/tenPow)) * tenPow; // 3874 -> 3870
      tempNumber = tempNumber - number; // 3874 - 3870 = 4

      System.out.println("number = " + number + "; tempNumber = " + tempNumber);

      if (Arrays.asList(this.decimals).contains(tempNumber)){
        result = result + this.romans[ Arrays.asList(this.decimals).indexOf(tempNumber) ];
        System.out.println("result = " + result);

      } else {
        int j = 0;

        while(j < decimals.length-1 && decimals[j] < tempNumber){ // nächstkleinere römische Zahl finden
          j++;
        }

        System.out.println("j = " + j);

        result = result + romans[j] + result;

        if (j < 2){
          result = result + romans[0].repeat( (int) (tempNumber / tenPow) );
        } else {
          result = result + romans[j-2].repeat( (tempNumber - decimals[j-2]) / tenPow );
        }
      }

      i++;
    }

    return result;
  }


  int RomanToInt(String number){
    int i, numberInt;
    numberInt = 0;

    // Hier verstehe ich Folgendes nicht: Ich möchte vom nullten bis zum vorletzten
    // Buchstaben von number iterieren. Da ich bei 0 zu zählen beginne, müsste der
    // letzte Buchstabe ja eigentlich den Index length()-1 haben, sodass der vorletzte
    // der mit Index length()-2 wäre. Trotzdem iteriert Java dann nur bis zum DRITTletzten
    // Buchstaben. Warum ist das so? 🤔

    for (i = 0; i < number.length() - 1; i++){
      String currentChar = Character.toString( number.charAt(i) );
      int indexOfChar = Arrays.asList(this.romans).indexOf( currentChar );
      int currentDecimal = this.decimals[indexOfChar];

      String successorChar = Character.toString( number.charAt(i+1) );
      int indexOfSuccChar = Arrays.asList(this.romans).indexOf( successorChar );
      int successorDecimal = this.decimals[indexOfSuccChar];

      if (currentDecimal >= successorDecimal) {
        numberInt += currentDecimal;
      } else {
        numberInt -= currentDecimal;
      }

      // System.out.println(number.charAt(i)); // Diese Ausgabe zeigt, dass length()-1 nur bis zum vorletzten i geht
    }

    // letzte römische Ziffer hat keinen Nachfolger, darum separate Behandlung
    int lastIndex = number.length() - 1;
    String lastChar = Character.toString( number.charAt(lastIndex) );
    int indexOfLastChar = Arrays.asList(this.romans).indexOf(lastChar);
    numberInt += this.decimals[ indexOfLastChar ];

    return numberInt;
  }


  Roman addition(Roman number){
    int normalResult = this.numberInt + number.numberInt;
    Roman romanResult = new Roman(normalResult);
    return romanResult;
  }


  Roman subtraction(Roman number){
    int normalResult = this.numberInt - number.numberInt;
    Roman romanResult = new Roman(normalResult);
    return romanResult;
  }


  Roman multiplication(Roman number){
    int normalResult = this.numberInt * number.numberInt;
    Roman romanResult = new Roman(normalResult);
    return romanResult;
  }


  Roman division(Roman number){
    int normalResult = (int) (this.numberInt / number.numberInt);
    Roman romanResult = new Roman(normalResult);
    return romanResult;
  }


  @Override public String toString(){
    return "Zahl " + this.hashCode() + ": römische Darstellung = " + this.numberString + " | Dezimaldarstellung = " + this.numberInt;
  }


  @Override public int hashCode(){
    return 0;
  }


  boolean equals(Roman number){
    return number.hashCode() == this.hashCode();
  }
}

public class RomanNumbers {
  public static void main(String[] args){
    System.out.print("Römische Zahl: ");
    Scanner scanner = new Scanner(System.in);
    String roman = scanner.next();
    Roman Roemisch1 = new Roman(roman);
    System.out.println(Roemisch1.toString());

    System.out.print("Dezimalzahl: ");
    int decimal = scanner.nextInt();
    Roman Roemisch2 = new Roman(decimal);
    System.out.println(Roemisch2.toString());
  }
}