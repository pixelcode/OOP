import java.util.LinkedList;

interface StringMenge {
  void add(String s);
  void remove(String s);
  boolean contains(String s);
  boolean isEmpty();
  int getSize();
  String[] getElements();
  int getCharCount();
  void print();
}

abstract class AbstrakteStringMenge implements StringMenge {
  public void add(String s) {}
  public void remove(String s) {}
  public boolean contains(String s) { return false; }
  public boolean isEmpty() { return false; }
  public int getSize() { return 0; }
  public String[] getElements() { return new String[] {}; }
  public int getCharCount() { return 0; }
  public void print() {}
}

class StringMengeImpl extends AbstrakteStringMenge implements StringMenge {
  LinkedList<String> strings;


  StringMengeImpl() {
    strings = new LinkedList<>();
  }


  public void add(String s) {
    this.strings.add(s);
  }


  public void remove(String s) {
    int i;

    for (i = 0; i < this.strings.size(); i++){
      if (this.strings.get(i) == s) {
        this.strings.remove(i);
        break;
      }
    }
  }


  public boolean contains(String s) {
    return this.strings.contains(s);
  }


  public boolean isEmpty() {
    if (getSize() == 0) return true;
    return false;
  }


  public int getSize() {
    return this.strings.size();
  }


  public String[] getElements() {
    String[] result = new String[this.strings.size()]; // Ausgabe bekommt gleiche Länge wie String-LinkedList
    int i;

    for (i = 0; i < this.strings.size(); i++){
      result[i] = this.strings.get(i);
    }

    return result;
  }


  public int getCharCount() {
    int charCount = 0;
    int i;

    for (i = 0; i < this.strings.size(); i++){
      charCount += this.strings.get(i).length();
    }

    return charCount;
  }


  public void print() {
    System.out.print(this.strings);
  }


  // könnte hilfreich sein 🤷
  public void print(boolean verbose){
    if (verbose){
      int i;
      System.out.println();

      for (i = 0; i < this.strings.size(); i++){
        System.out.println(this.strings.get(i));
      }

      System.out.println();

    } else {
      this.print();
    }
  }
}


public class TestMenge {
  public static void main(String args[]) {
    java.util.Random rand = new java.util.Random();
    StringMenge sm = new StringMengeImpl();

    while (sm.getSize() < 5) {
      String r = "Eingabe" + String.valueOf( rand.nextInt(5) );
      boolean c = sm.contains(r);
      sm.add(r);

      System.out.printf(
        "Element %3s ist %15s, Mengen-Größe ist %2d, mit: ",
        r,
        ((c ? "" : "nicht ") + "vorhanden"),
        sm.getSize()
      );

      sm.print();

      System.out.print(", Zeichenanzahl = " + sm.getCharCount());
      System.out.println();
    }
  }
}