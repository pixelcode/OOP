import java.util.Scanner;
import java.util.Objects;

// ----
// Flug
// ----
class Flight {
  int flightNumber;   // Flugnummer
  String location;    // Abflugs-/Zielort
  String gate;        // Gate
  String time;        // Abflugs-/Ankunftszeit
  boolean inOut;      // eingehender (true) oder abgehender (false) Flug
}

// ---------
// Flughafen
// ---------
class Airport {
  private int maxFlights;
  private Flight[] flights;

  Airport(int maxFlights){
    this.maxFlights = maxFlights;
    flights = new Flight[maxFlights]; // Array mit maxFlights Einträgen initialisieren
  }

  void addNewFlight(Flight flight){
    int flightCount = 0;
    int i;
    boolean flightNumberTaken = false;

    for (i = 0; i < flights.length; i++){
      if (flights[i] != null){
        flightCount++; // Wie viele Flüge sind tatsächlich gespeichert?

        if (flights[i].flightNumber == flight.flightNumber){ // Ist Flugnummer bereits vorhanden?
          System.out.println(" Fehler: Flug " + flight.flightNumber + " bereits vorhanden!");
          flightNumberTaken = true;
          break; // Flug bereits vorhanden => Flugzählung obsolet => Abbruch
        }
      }
    }

    if (flightCount + 1 > maxFlights) {
      System.out.println(" Fehler: Maximal " + maxFlights + " Flüge erlaubt!");

    } else if (!(flightNumberTaken)){
      flights[flightCount] = flight;
      System.out.println(" Flug " + flight.flightNumber + " erfolgreich hinzugefügt.");
    }
  }

  void removeFlight(int flightNumber){
    int i;
    boolean flightRemoved = false;

    for (i = 0; i < flights.length; i++){ // Welcher Flug hat die zu entfernende Flugnummer?
      if (flights[i] != null && flights[i].flightNumber == flightNumber){ // zuerst NullPointerException vermeiden
        flights[i] = null;
        flightRemoved = true;
        System.out.println(" Flug " + flightNumber + " erfolgreich gelöscht.");
      }
    }

    if (!(flightRemoved)) System.out.println(" Fehler: Flug " + flightNumber + " nicht gefunden, also nicht gelöscht.");
  }

  // --------------------------
  // Flüge typabhängig ausgeben
  // --------------------------
  void listOnScreen(boolean inOut){
    int i;
    int flightCount = 0;

    for (i = 0; i < flights.length; i++){
      if (flights[i] != null && flights[i].inOut == inOut){ // zuerst NullPointerException vermeiden
        System.out.println(flights[i].flightNumber + " | " + flights[i].location + " | " + flights[i].time + " | " + flights[i].gate);
        flightCount++; // Wie viele Flüge mit übereinstimmender Abflugs-/Ankunftsangabe sind gespeichert?
      }
    }

    if (flightCount == 0){
      System.out.println(" Fehler: Keine Flüge verfügbar!");
    }
  }

  // -----------------------
  // Abflüge ausgeben lassen
  // -----------------------
  void listDeparturesOnScreen(){
    System.out.println("-----------------------------");
    System.out.println(" Flug | Ziel | Abflug | Gate ");
    System.out.println("-----------------------------");
    listOnScreen(false); // inOut = false <=> Abflug
  }

  // ------------------------
  // Ankünfte ausgeben lassen
  // ------------------------
  void listArrivalsOnScreen(){
    System.out.println("------------------------------------");
    System.out.println(" Flug | Abflugsort | Ankunft | Gate ");
    System.out.println("------------------------------------");
    listOnScreen(true); // inout = true <=> Ankunft
  }
}

// -------------
// Hauptprogramm
// -------------
public class AirportDisplay {

  // ---------
  // Hauptmenü
  // ---------
  static void mainMenu(Airport airport){
    System.out.print("\n d: Abflüge anzeigen \n a: Ankünfte anzeigen \n n: Neuer Flug \n r: Flug löschen \n x: Beenden \n \n Aktion: ");
    Scanner scanner = new Scanner(System.in);
    String action = scanner.next();
    System.out.println("");

    // ------------------
    // Menüauswahl prüfen
    // ------------------
    if (Objects.equals(action,"d")){ // departures
      airport.listDeparturesOnScreen();

    } else if (Objects.equals(action,"a")){ // arrivals
      airport.listArrivalsOnScreen();

    } else if (Objects.equals(action,"n")){ // new

      System.out.print(" Flugnummer: ");
      int flightNumber = In.readInt();

      boolean inOut;
      String departArrivLocStr, departArrivTimeStr;

      System.out.print(" Ankunft (a) oder Abflug (d)? (Standard: Ankunft) ");
      String inOutStr = scanner.next();

      if (Objects.equals(inOutStr, "d")){
        inOut = false;
        departArrivLocStr = " Zielort";
        departArrivTimeStr = " Abflugszeit";
      } else {
        inOut = true;
        departArrivLocStr = " Abflugsort";
        departArrivTimeStr = " Ankunftszeit";
      }

      System.out.print(departArrivLocStr + ": ");
      String location = scanner.next();

      System.out.print(departArrivTimeStr + ": ");
      String time = scanner.next();

      System.out.print(" Gate: ");
      String gate = scanner.next();
      System.out.println("");

      Flight newFlight;
      newFlight = new Flight();

      newFlight.flightNumber = flightNumber;
      newFlight.location = location;
      newFlight.gate = gate;
      newFlight.time = time;
      newFlight.inOut = inOut;

      airport.addNewFlight(newFlight);

    } else if (Objects.equals(action,"r")){ // remove

      System.out.print(" Flug löschen. Flugnummer: ");
      int flightNumber = In.readInt();
      airport.removeFlight(flightNumber);

    } else if (Objects.equals(action,"x")){ // exit
      System.exit(0);
    }

    mainMenu(airport); // Menü immer wieder anzeigen
  }

  // -------------
  // Programmstart
  // -------------
  public static void main(String args[]){
    System.out.println(" _____________________________________");
    System.out.println(" VERWALTUNG DER FLUGHAFEN-ANZEIGETAFEL");
    System.out.println(" *************************************");

    System.out.print("\n Maximale Anzahl Flüge: ");
    int maxFlights = In.readInt();

    Airport newAirport;

    if (maxFlights >= 1){
      newAirport = new Airport(maxFlights);
      mainMenu(newAirport);
    } else {
      System.out.println(" Fehler: System muss mindestens einen Flug verarbeiten können!");
    }
  }
}