public class ShowContainsZeroAction implements ActionObject {
  private boolean containsZero = false;

  boolean getContainsZero() {
    return this.containsZero;
  }

  public void action(Node n) {
    if (n.data instanceof Integer) {
      Integer tmp = (Integer) n.data;
      if (tmp.intValue() == 0) containsZero = true;
    }
  }
}