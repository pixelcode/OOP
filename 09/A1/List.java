public class List {
  private Node head;

  List() {
    this.head = new Node();
    this.head.data = new Object();
    this.head.next = null;
  }

  void add(Object value) {
    Node newNode = new Node();
    newNode.data = value;
    newNode.next = null;

    Node lastNode = new Node();
    lastNode = this.head;

    for (Node cursor = head; cursor.next != null; cursor = cursor.next) {
      lastNode = cursor.next;
    }

    lastNode.next = newNode;
  }

  public void traverseAndApply(ActionObject p){
    for (Node cursor = head; cursor != null; cursor = cursor.next) {
      p.action(cursor);
    }
  }
}