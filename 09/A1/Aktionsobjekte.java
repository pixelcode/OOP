public class Aktionsobjekte {
  public static void showContainsZero(List list) {
    ShowContainsZeroAction s = new ShowContainsZeroAction();
    list.traverseAndApply(s);

    if (s.getContainsZero()) System.out.println("Liste enthält 0!");
    else System.out.println("Liste enthält 0 nicht!");
  }

  public static void showPosSum(List list) {
    SumAction s = new SumAction();
    list.traverseAndApply(s);
    System.out.println("Summe = " + s.getSum());
  }

  public static void main(String[] args){
    List list = new List();

    list.add(1);
    list.add(2);
    list.add(3);
    list.add(4);
    list.add(5);
    list.add(-4);
    list.add(-6.9f);
    list.add('c');
    list.add("lorem ipsum");
    list.add(0);

    showContainsZero(list);
    showPosSum(list);
  }
}