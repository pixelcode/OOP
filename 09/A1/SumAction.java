public class SumAction implements ActionObject{
  private int sum = 0;

  int getSum() {
    return this.sum;
  }

  public void action(Node n) {
    if(n.data instanceof Integer) {
      Integer tmp = (Integer) n.data;
      if (tmp.intValue() >= 0) sum += tmp.intValue();
    }
  }
}