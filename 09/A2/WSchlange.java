import java.util.LinkedList;
import java.util.Queue;

public class WSchlange {
    private Queue<Object> wSchlange;

    WSchlange(){
        this.wSchlange = new LinkedList<Object>();
    }

    Queue<Object> getWSchlange(){
        return this.wSchlange;
    }

    void add(Object element){
        this.wSchlange.add(element);
    }

    Object remove(){
        return this.wSchlange.remove();
    }
}
