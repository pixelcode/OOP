public class Warteschlange {
	public static void main(String[] args){
		Schlange<String> stringSchlange = new Schlange<String>();
		stringSchlange.add("hello world");
		stringSchlange.add("ciao world");

		Schlange<Double> doubleSchlange = new Schlange<Double>();
		doubleSchlange.add(3.141);
		doubleSchlange.add(1.234);

		WSchlange objectSchlange = new WSchlange();
		objectSchlange.add(6.96);
		objectSchlange.add("this is a string");

		System.out.println(stringSchlange.getSchlange());
		System.out.println(doubleSchlange.getSchlange());
		System.out.println(objectSchlange.getWSchlange());

		System.out.println(stringSchlange.remove());
		System.out.println(doubleSchlange.remove());
		System.out.println(objectSchlange.remove());
	}
}
