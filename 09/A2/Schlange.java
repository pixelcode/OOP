import java.util.LinkedList;
import java.util.Queue;

public class Schlange<T> {
	private Queue<T> schlange;

	Schlange(){
		this.schlange = new LinkedList<T>();
	}

	Queue<T> getSchlange(){
		return this.schlange;
	}

	void add(T element){
		this.schlange.add(element);
	}

	T remove(){
		return this.schlange.remove();
	}
}
