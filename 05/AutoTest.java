import java.lang.Math;

class Auto {
  protected String kennzeichen;
  protected int kilometerstand, sitzplaetze;
  protected boolean antenne;
  // Ist hier `protected` zur Datenkapselung i.O.?
  // Oder müsste es mit `private` gemacht und dann in `PickUp` re-implementiert werden?



  Auto(){
    this.kilometerstand = 0;
    this.kennzeichen = "J-AA 01";
    this.sitzplaetze = 5;
    this.antenne = false;
  }



  Auto(String kennzeichen){
    this.kilometerstand = 0;
    this.sitzplaetze = 5;
    this.antenne = false;

    if (kennzeichen != ""){
      this.kennzeichen = kennzeichen;
    } else {
      this.kennzeichen = "J-AA 01";
    }
  }



  Auto(boolean roadster){
    this.kilometerstand = 0;
    this.kennzeichen = "J-AA 01";
    this.antenne = false;

    if (roadster) this.sitzplaetze = 2; else this.sitzplaetze = 5;
  }



  Auto(String kennzeichen, boolean roadster){
    this.kilometerstand = 0;
    this.antenne = false;

    if (kennzeichen != ""){
      this.kennzeichen = kennzeichen;
    } else {
      this.kennzeichen = "J-AA 01";
    }

    if (roadster) this.sitzplaetze = 2; else this.sitzplaetze = 5;
  }



  String getKennzeichen(){
    return this.kennzeichen;
  }



  int getKilometerstand(){
    return this.kilometerstand;
  }



  int getSitzplaetze(){
    return this.sitzplaetze;
  }



  void fahre(int kilometer){
    if (kilometer > 0){
      this.kilometerstand += kilometer;
      System.out.println("Auto " + this.getKennzeichen() + " hat erfolgreich " + kilometer + " Kilometer zurückgelegt.");
    } else {
      System.out.println("Autos müssen mehr als 0 Kilometer fahren!");
    }
  }



  void fahreAntenneAus(){
    if (this.antenne != true){
      System.out.println("Antenne wird ausgefahren.");
      this.antenne = true;
    } else {
      System.out.println("Antenne bereits ausgefahren.");
    }
  }



  void fahreAntenneEin(){
    if (this.antenne != false){
      System.out.println("Antenne wird eingefahren.");
      this.antenne = false;
    } else {
      System.out.println("Antenne bereits eingefahren.");
    }
  }



  void bereiteWaschenVor(){
    this.antenne = false;
  }



  void wasche(){
    this.bereiteWaschenVor();
    System.out.println("Auto " + this.getKennzeichen() + " wird gewaschen.");
  }



  @Override public String toString(){
    String antennenStatus;
    if (this.antenne) antennenStatus = "ausgefahren"; else antennenStatus = "eingefahren";

    String typ;
    if (this.sitzplaetze == 5) typ = "Auto"; else typ = "Roadster";

    return "Typ: " + typ + "; Kennzeichen: " + this.getKennzeichen() +
      "; Kilometerstand: " + this.getKilometerstand() +
      "; Sitzplätze: " + this.getSitzplaetze() +
      "; Antenne: " + antennenStatus;
  }
}



// ------
// PICKUP
// ------

class PickUp extends Auto {
  private int f, ladung;



  PickUp(int fassungsvermoegen){
    this.f = fassungsvermoegen;
    this.sitzplaetze = 2;
    this.ladung = 0;
    this.kilometerstand = 0;
    this.kennzeichen = "J-AA 01";
    this.antenne = false;
  }



  PickUp(String kennzeichen, int fassungsvermoegen){
    this.f = fassungsvermoegen;
    this.sitzplaetze = 2;
    this.ladung = 0;
    this.kilometerstand = 0;
    this.antenne = false;

    if (kennzeichen != ""){
      this.kennzeichen = kennzeichen;
    } else {
      this.kennzeichen = "J-AA 01";
    }
  }



  int getLadung(){
    return this.ladung;
  }



  boolean beladen(int ladung){
    int ladungVorher = this.getLadung();

    if (ladung + this.getLadung() <= f){
      this.ladung += ladung;
      System.out.println("Inhalt der Ladefläche von " + this.getKennzeichen() +
        " von " + ladungVorher + " um " + ladung + " auf " + this.getLadung() + " erhöht.");
      return true;

    } else {
      System.out.println("Inhalt der Ladefläche von " + this.getKennzeichen() +
        " konnte NICHT von " + ladungVorher + " um " + ladung + " erhöht werden.");
      return false;
    }
  }



  void entladen(int ladung){
    int ladungVorher = this.getLadung();
    this.ladung = Math.max(0, this.getLadung() - ladung);

    System.out.println("Inhalt der Ladefläche von " + this.getKennzeichen() +
      " von " + ladungVorher + " um " + ladung + " auf " + this.getLadung() +  " reduziert.");
  }



  void entladen(){
    this.entladen(this.getLadung());
  }



  void bereiteWaschenVor(){
    this.antenne = false;
    this.entladen();
  }



  @Override public String toString(){
    String antennenStatus;
    if (this.antenne) antennenStatus = "ausgefahren"; else antennenStatus = "eingefahren";

    return "Typ: Pick-up; Kennzeichen: " + this.getKennzeichen() +
      "; Kilometerstand: " + this.getKilometerstand() +
      "; Sitzplätze: " + this.getSitzplaetze() +
      "; Fassungsvermögen: " + this.f +
      "; Ladung: " + this.getLadung() +
      "; Antenne: " + antennenStatus;
  }
}

// --------
// AUTOTEST
// --------

public class AutoTest {
  public static void main(String args[]){
    Auto[] autos = new Auto[6];

    autos[0] = new Auto();
    autos[1] = new Auto("0-1");
    autos[2] = new Auto(true);
    autos[3] = new Auto("J-EN4", false);
    autos[4] = new PickUp(500);
    autos[5] = new PickUp("D-093-420", 800);

    int i;

    for (i = 0; i < 6; i++){
      System.out.println(autos[i].toString() + "\n");
    }

    System.out.println();

    for (i = 0; i < 6; i++){
      int randFahrstrecke = (int)(Math.random() * 2000) + 1;
      int randLadung = (int)(Math.random() * 900) + 1;
      int randEntladung = (int)(Math.random() * 900) + 1;

      autos[i].fahre(randFahrstrecke);
      autos[i].bereiteWaschenVor();
      autos[i].wasche();
      autos[i].fahreAntenneAus(); // Wird die ausgefahrene Antenne beim „Kopieren“ eines Pick-ups übernommen? (s. Z. 278)

      if (autos[i] instanceof PickUp){
        System.out.println();

        PickUp hilfsPickUp = (PickUp)autos[i]; // https://stackoverflow.com/a/21446604

        hilfsPickUp.beladen(randLadung);
        hilfsPickUp.entladen(randEntladung);
        hilfsPickUp.entladen();

        autos[i] = hilfsPickUp; // Das funktioniert „on my machine“ – aber ist das auch „guter Stil“?
      }

      System.out.println();
    }

    for (i = 0; i < 6; i++){
      System.out.println(autos[i].toString() + "\n");
    }
  }
}