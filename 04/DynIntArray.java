public class DynIntArray {
  void add(int e) {}
  void setElementAt(int i, int e) {}

  int getElementAt(int i) {
    return 0;
  }

  int getElementCount() {
    return 0;
  }

  void print() {}



  public static void main(String args[]) {
    for (int i = 0; i < 2; i++) {
      DynIntArray da = null;

      if (i == 0) {
        da = new DIAarray();
      } else if (i == 1) {
        da = new DIAlist();
      }

      da.add(4);
      da.add(8);
      da.add(10);
      da.add(1);
      da.print();

      da.setElementAt(0, 0);
      da.add(5);

      da.setElementAt(2, da.getElementAt(2) + 10);
      da.print();

      System.out.println("DIA: elements=" + da.getElementCount() +
        " da[4]=" + da.getElementAt(4) +
        " da[9]=" + da.getElementAt(9));
    }
  }
}






class DIAarray extends DynIntArray {
  int[] ints;

  DIAarray() {
    ints = new int[0];
  }



  @Override void add(int e){
    int[] intshelp = ints; // helper variable to temporarily store the existing array
    ints = new int[intshelp.length + 1]; // increase the length of the existing array by overriding it with a larger one
    int i;

    if (intshelp.length > 0){ // if existing array isn't empty
      for (i = 0; i < intshelp.length; i++){
        ints[i] = intshelp[i];  // copy existing array to new array
      }

      ints[intshelp.length] = e; // add new element to bottom
    } else { // if existing array is empty, then just store the new element to the only index
      ints[0] = e;
    }
  }



  @Override void setElementAt(int i, int e){
    if (i >= 0 && i < ints.length){
      ints[i] = e;
    }
  }



  @Override int getElementAt(int i){
    if (i >= 0 && i < ints.length){
      return ints[i];
    } else {
      return 0;
    }
  }



  @Override int getElementCount(){
    return ints.length;
  }



  @Override void print(){
    int i;
    System.out.print("[");

    for (i = 0; i < ints.length - 1; i++){
      System.out.print("" + ints[i] + ", ");
    }

    // We need to manually ouput the last item in order to avoid an additional delimiter (comma).
    System.out.println(ints[i] + "]");
  }
}






class DIAListItem {
  private int data;
  private DIAListItem successor;



  DIAListItem(){
    this.successor = null; // If a LinkedList item doesn't have a successor, then we simply set it to null.
  }



  void setData(int data){
    this.data = data;
  }



  void setSuccessor(DIAListItem successor){
    this.successor = successor;
  }



  int getData(){
    return this.data;
  }



  DIAListItem getSuccessor(){
    return this.successor;
  }
}






class DIAlist extends DynIntArray {
  DIAListItem head;

  @Override void add(int e){
    DIAListItem newElement = new DIAListItem();
    newElement.setData(e);
    newElement.setSuccessor(null); // our new element is going to be the last item in the LinkedList, so it must point to null

    if (head == null){
      this.head = newElement;
    } else {
      DIAListItem last = this.head;

      while (last.getSuccessor() != null){ // only stop if successor is null (which is the definition for a LinkedList's last item)
        last = last.getSuccessor(); // follow the “chain” by examining the current item’s successor
      }

      last.setSuccessor(newElement); // append the new element
    }
  }



  @Override void setElementAt(int i, int e){
    DIAListItem currentItem = this.head;
    DIAListItem newItem = new DIAListItem();
    newItem.setData(e);
    int counter = 0;

    // iterate through LinkedList just like in the add() procedure
    while (i >= 0 && counter < i && currentItem.getSuccessor() != null){
      currentItem = currentItem.getSuccessor();
      counter++;
    }

    currentItem.setData(e);
  }



  @Override int getElementAt(int i){
    DIAListItem currentItem = this.head;
    int counter = 0;

    if (i < 0 || i > getElementCount()){ // LinkedList starts at index 0 and ends at index count-1
      return 0;
    }

    // iterate through LinkedList just like in the add() procedure
    while (counter < i && currentItem.getSuccessor() != null){
      currentItem = currentItem.getSuccessor();
      counter++;
    }

    return currentItem.getData();
  }



  @Override int getElementCount(){
    int counter = 0;
    DIAListItem last = this.head;

    // iterate through LinkedList just like in the add() procedure
    while (last != null){ // count items that exist / aren't null
      last = last.getSuccessor();
      counter++;
    }

    return counter;
  }



  @Override void print(){
    DIAListItem last = this.head;
    // We need to manually start with the first list item to avoid having an additional delimiter (comma).
    System.out.print("[" + last.getData());
    last = last.getSuccessor();

    // iterate through LinkedList just like in the add() procedure
    while (last != null){
      System.out.print(", " + last.getData());
      last = last.getSuccessor();
    }

    System.out.println("]");
  }
}