import java.lang.Math;

class Einwohner {
  protected int einkommen;
  protected double steuersatz = 0.1;
  protected int mindeststeuer = 1;
  protected int steuerfreibetrag = 0;
  
  int zuVersteuerndesEinkommen(){
    return Math.max(this.einkommen - this.steuerfreibetrag, 0);
  }

  int steuer(){
    int steuern = (int) (zuVersteuerndesEinkommen() * this.steuersatz);
    return Math.max(steuern, this.mindeststeuer);
  }

  void setEinkommen(int einkommen){
    this.einkommen = einkommen;
  }
}

class Adel extends Einwohner {
  public static final int MINDESTSTEUER = 20;

  Adel(){
    this.mindeststeuer = this.MINDESTSTEUER;
  }
}

class Koenig extends Einwohner {
  private static final double STEUERSATZ = 0;
  private static final int MINDESTSTEUER = 0;

  Koenig(){
    this.steuersatz = this.STEUERSATZ;
    this.mindeststeuer = this.MINDESTSTEUER;
  }
}

class Bauer extends Einwohner {

}

class Leibeigener extends Bauer {
  public static final int STEUERFREIBETRAG = 12;

  Leibeigener(){
    this.steuerfreibetrag = this.STEUERFREIBETRAG;
  }
}

public class Koenigreich {
  public static void main(String args[]){
    Leibeigener leibeigener = new Leibeigener();
    leibeigener.setEinkommen(100);
    System.out.println("Steuerfreibetrag: " + leibeigener.steuerfreibetrag);
    System.out.println("zu versteuerndes Einkommen: " + leibeigener.zuVersteuerndesEinkommen());
    System.out.println("Steuer: " + leibeigener.steuer());
  }
}